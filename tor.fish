if ! test -f $sugar_install_directory/config/tor.fish
  cp $argv[1]/config/tor.fish $sugar_install_directory/config/tor.fish
end

source $sugar_install_directory/config/tor.fish

function torssh -d 'SSH over the Tor network.'
  ssh -o ProxyCommand="nc -x $tor_server:$tor_port %h %p" $argv
end

function torcurl -d 'Curl over the Tor network.'
  curl --proxy "socks5h://$tor_server:$tor_port" $argv
end
